CLIENTS=`/bin/ls -1 /logfiles/`
for c in ${CLIENTS[@]}
do
	ENVS=`/bin/ls -1 "/logfiles/$c/" | /bin/grep "${c}prd"`
	for e in ${ENVS[@]}
	do
		gunzip -k -c "/logfiles/$c/$e/$c/live/modsec_audit.log-"* > "modsec_audit.log.$e"
		cat "/logfiles/$c/$e/$c/live/modsec_audit.log" >> "modsec_audit.log.$e"
		perl "/sites/$c/live/core/import/scripts/mod_security_log_parse.pl" "/sites/$c/live/" "modsec_audit.log.$e"
	done
done
