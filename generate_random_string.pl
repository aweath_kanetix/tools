#!/usr/bin/perl

# generates a random alphanumeric string with length [8..20] characters; character set and string length can be easily changed by editing this file

use strict;
use warnings;

my @chars = (0..9,'A'..'Z','a'..'z');
my $length = 8 + int(rand(12));

print join('', map {$chars[int(rand(scalar(@chars)))]} (1..$length))."\n";
