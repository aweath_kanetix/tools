#!/bin/bash

# Creates an Oracle Wallet, populates it based on the provided schema names, and creates tnsnames.ora entries for those connections.
# Clobbers any existing wallet and tnsnames.ora files.
# Expects /opt/oracle/wallet/ and /opt/oracle/product/instantclient_12_1/network/admin/ to already exist with appropriate permissions.
# Input should be a list of schema names.

# Clear out any old files
rm -f /opt/oracle/wallet/*
rm -f /opt/oracle/product/instantclient_12_1/network/admin/tnsnames.ora

# Create wallet
while true; do echo $WALLETPW; done | mkstore -wrl "/opt/oracle/wallet" -create

# Create bash array for wallet entries
oracle_username=( $@ )
oracle_connection_string=()

# Create wallet entries and generate tnsnames.ora
for i in "${!oracle_username[@]}"
do
	oracle_connection_string+=("`echo ${ORACLE_SERVICENAME}_${oracle_username[i]} | tr '[:upper:]' '[:lower:]'`")
	echo $WALLETPW | mkstore -wrl "/opt/oracle/wallet" -createCredential "${oracle_connection_string[i]}" "$ORACLE_USERNAME[${oracle_username[i]}]" "$ORACLE_PASSWORD"
	echo "${oracle_connection_string[i]} = (DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = $ORACLE_DBHOST)(PORT = 1521))(CONNECT_DATA = (SERVER = DEDICATED)(SERVICE_NAME = $ORACLE_SERVICENAME)))" >> /opt/oracle/product/instantclient_12_1/network/admin/tnsnames.ora
done

# Ensure other devs have read access to the wallet files
chmod 660 /opt/oracle/wallet/*
