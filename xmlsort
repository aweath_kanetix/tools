#!/usr/bin/perl

# script for taking an XML document and re-ordering sibling nodes to be in sorted order; useful e.g. for comparing XML documents generated from different processes
# e.g
# <foo>
#  <ringo></ringo>
#  <george>harrison</george>
#  <paul/>
#  <john>
#    <yoko/>
#    <lennon/>
#    <ono/>
#  </john>
# </foo>
# becomes
# <foo>
#  <george>harrison</george>
#  <john>
#    <lennon/>
#    <ono/>
#    <yoko/>
#  </john>
#  <paul/>
#  <ringo></ringo>
# </foo>

use XML::LibXML;

my $skipEmptyTextNodes = 1;

my $parser = XML::LibXML->new();
$XML::LibXML::skipXMLDeclaration = 1;
undef $/;
my $xml = <STDIN>;
my $doc = eval { $parser->parse_string($xml); };
die ($@) if ($@);
xmlsort($doc->documentElement());
print STDOUT $doc->toString()."\n";

sub xmlsort {
	my ($node) = @_;

	my @nodes = $node->childNodes();
	foreach my $child (@nodes) {
		$node->removeChild($child);
		xmlsort($child);
	}
	foreach my $child (sort {$a->nodeName() cmp $b->nodeName() || $a->toString() cmp $b->toString()} @nodes) {
		next if ($skipEmptyTextNodes && $child->nodeValue() =~ /^\s+$/);
		$node->appendChild($child);
	}

	my @attrs = grep {$_->nodeType() == XML_ATTRIBUTE_NODE} $node->attributes();
	foreach my $attr (@attrs) {
		$node->removeAttribute($attr->nodeName());
	}
	foreach my $attr (sort {$a->nodeName() cmp $b->nodeName()} @attrs) {
		$node->setAttribute($attr->nodeName(), $attr->nodeValue());
	}
}
