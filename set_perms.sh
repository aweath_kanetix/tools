#!/bin/sh

# Our umask setting allows group write to be set on new files/dirs,
# and usually this is what we want when working with shared files,
# but for one's home directory, it's not such a great idea.

# We'll apply a file access control list to remove group write from new files/dirs:
setfacl --recursive -d --set g::r-X .
# And then recursively strip any existing group write permissions from all files/dirs:
chmod -R g-w .

# Note that this only needs to be done once when setting up a directory.
# The access control lists for a file/directory can be checked with getfacl.
