#!/bin/bash
CLIENT=`echo $HOSTNAME|awk -F2?qa {'print $1'}`
SQL="set heading off pagesize 0 tab on trimout on;\nselect sys_context('USERENV','SERVER_HOST')||':'||chr(9)||to_char(systimestamp at time zone 'AMERICA/TORONTO', 'Dy Mon DD HH24:MI:SS TZD YYYY') from dual;\nexit;"
echo -e "start:\t`date`";
ssh ${CLIENT}dev "echo -e \"\$HOSTNAME:\\t\`date\`\"; echo -e \"$SQL\" | sqlplus -s /@extdev_${CLIENT}_dev1;";
echo -e "$HOSTNAME:\t`date`";
echo -e "$SQL" | sqlplus -s /@extqadb_${CLIENT}_qa;
ssh ${CLIENT}stage "echo -e \"\$HOSTNAME:\\t\`date\`\"; echo -e \"$SQL\" | sqlplus -s /@kandb2rac_${CLIENT}_live;";
echo -e "end:\t`date`"
