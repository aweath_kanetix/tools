#!/bin/bash

if [ "$1" = "" ]
then
	echo "Usage:"
	echo "  f <search text> [<base dir or files>]"
	echo "Calls 'grep -r \"\$1\" \$2' using grep alias defined in ~/.aliases"
exit
fi

GREP=$1
shift
BASE=$@

if [ "$BASE" = "" ]
then
	BASE=.
fi

if [ -e ~/.aliases ]
then
	shopt -s expand_aliases
	source ~/.aliases
fi
grep -r "$GREP" $BASE
